#!/bin/bash
apt-get update
apt-get -y  install curl apt-transport-https ca-certificates
#add docker gpg
curl -fsSL https://yum.dockerproject.org/gpg |  apt-key add -
#add docker repo
echo  "deb https://apt.dockerproject.org/repo/ \
       debian-$(lsb_release -cs) \
       main" >>/etc/apt/sources.list 
myip=
while IFS=$': \t' read -a line ;do
    [ -z "${line%inet}" ] && ip=${line[${#line[1]}>4?1:2]} &&
        [ "${ip#127.0.0.1}" ] && myip=$ip
  done< <(LANG=C /sbin/ifconfig)
apt-get update
apt-get install -y  docker-engine
clear
read -p  "Set Container Name: " container_name
read -p 'Set Root password: ' pass
read -p 'Set ssh port: ' ssh
read -p 'Set postgres user: ' postgres_user
read -p 'Set postgress password: '  postgress_password
read -p 'Set postgress Database name: ' postgress_db
read -p 'Set apache web port: '  web_port

cp -r ./template build
echo $postgres_user
sed -i "s/ROOT_PASSWORD/$pass/g" 		./build/Dockerfile 
sed -i "s/POSTGRESUSER/$postgres_user/g" 	./build/Dockerfile
sed -i "s/POSTGRESPASS/$postgress_password/g"  ./build/Dockerfile
sed -i "s/POSTGRESDB/$postgress_db/g" 		./build/Dockerfile



cd build
docker build -t test/postgres ./
docker run   --name $container_name  -p $web_port:80 -p $ssh:22 -d  test/postgres /bin/bash /home/start.sh
echo \n
clear
echo Your  Server $container_name is ready
echo Root Password: $pass
echo SSH password: $pass
echo Postgres User: $postgres_user
echo Postgres Password: $postgress_password
echo Postgres DatabaseName: $postgress_db
echo For connect You can use http://$myip:$web_port
cd ..
rm -rf build
